> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Kristopher Mangahas

### Assignment 4 # Requirements

*Deliverables:*

1. Provide Bitbucket read-only access to a4 repo, *must* include README.md, using Markdown syntax
2. Blackboard Links: a4 Bitbucket repo



#### Assignment Screenshots:

*Screenshot of a passed validation*:

![Passed validation Screenshot](img/passed.png)


*Screenshot of failing due to null entries inputs*:

![Null Entry Failure Screenshot](img/null.png)


*Screenshot of Client Side Verification*:

![Client Side VerificationScreenshot](img/client.png)

